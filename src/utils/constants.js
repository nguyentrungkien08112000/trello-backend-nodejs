// Những domains được phép truy cập đến tài nguyên của server
const WHITELIST_DOMAINS = [
  'http://localhost:5173'
]

export { WHITELIST_DOMAINS }
